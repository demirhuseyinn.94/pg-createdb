Role Name and Description
=========

This role is responsible for applying some database administrator post configuration to an existing database. This role can be extended whenever a new configuration has to be applied to an existing database

1. Create required roles on database
2. Create monitoring views on database
3. Create template database as template
4. Create application database which is specified
5. Create application database user which is specified

Requirements
------------

Please make sure you've installed the following ansible collection on ansible server

```bash
ansible-galaxy collection install community.postgresql
ansible-galaxy collection install community.general
```

Please make sure you've installed the following pip package on target server

```bash
pip3 install psycopg2-binary
```

Role Variables
--------------

| variableName          | exampleValue | Description                       |
|-----------------------|--------------|-----------------------------------|
| database_name           | demo    |   |
| template           | trtemplate         |               |
| app_sec               | randomtext          |  |

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yml

---
- hosts: localhost
  connection: local
  become: true
  roles:
    - role: pgs-createdb
      vars:
        database_name: demo
        template: template0
        app_sec: randomtokencomingfromconsul


```

License
-------

BSD

Author Information
------------------

* Hüseyin DEMİR