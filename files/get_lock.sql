CREATE OR REPLACE VIEW dba_get_vacuum_progress AS
SELECT p.pid,
    now() - a.xact_start AS duration,
    coalesce(wait_event_type || '.' || wait_event, 'f') AS waiting,
    CASE
        WHEN a.query ~* '^autovacuum.*to prevent wraparound' THEN 'wraparound'
        WHEN a.query ~* '^vacuum' THEN 'user'
        ELSE 'regular'
    END AS mode,
    p.datname AS database,
    p.relid::regclass AS table,
    p.phase,
    pg_size_pretty(
        p.heap_blks_total * current_setting('block_size')::int
    ) AS table_size,
    pg_size_pretty(pg_total_relation_size(relid)) AS total_size,
    pg_size_pretty(
        p.heap_blks_scanned * current_setting('block_size')::int
    ) AS scanned,
    pg_size_pretty(
        p.heap_blks_vacuumed * current_setting('block_size')::int
    ) AS vacuumed,
    round(
        100.0 * p.heap_blks_scanned / p.heap_blks_total,
        1
    ) AS scanned_pct,
    round(
        100.0 * p.heap_blks_vacuumed / p.heap_blks_total,
        1
    ) AS vacuumed_pct,
    p.index_vacuum_count,
    round(100.0 * p.num_dead_tuples / p.max_dead_tuples, 1) AS dead_pct
FROM pg_stat_progress_vacuum p
    JOIN pg_stat_activity a using (pid)
ORDER BY now() - a.xact_start DESC;



CREATE OR REPLACE VIEW dba_get_lockings AS
SELECT COALESCE(l1.relation::regclass::text, l1.locktype) as locked_item,
    w.wait_event_type as waiting_ev_type,
    w.wait_event as waiting_ev,
    w.query as waiting_query,
    l1.mode as waiting_mode,
    (
        select now() - xact_start as waiting_xact_duration
        from pg_stat_activity
        where pid = w.pid
    ),
    (
        select now() - query_start as waiting_query_duration
        from pg_stat_activity
        where pid = w.pid
    ),
    w.pid as waiting_pid,
    w.usename as waiting_user,
    w.state as waiting_state,
    l.wait_event_type as locking_ev_type,
    l.wait_event_type as locking_ev,
    l.query as locking_query,
    l2.mode as locking_mode,
    (
        select now() - xact_start as locking_xact_duration
        from pg_stat_activity
        where pid = l.pid
    ),
    (
        select now() - query_start as locking_query_duration
        from pg_stat_activity
        where pid = l.pid
    ),
    l.pid as locking_pid,
    l.usename as locking_user,
    l.state as locking_state
FROM pg_stat_activity w
    JOIN pg_locks l1 ON w.pid = l1.pid
    AND NOT l1.granted
    JOIN pg_locks l2 ON (
        l1.transactionid = l2.transactionid
        AND l1.pid != l2.pid
    )
    OR (
        l1.database = l2.database
        AND l1.relation = l2.relation
        and l1.pid != l2.pid
    )
    JOIN pg_stat_activity l ON l2.pid = l.pid
WHERE w.wait_event is not null
    and w.wait_event_type is not null
ORDER BY l.query_start,
    w.query_start;